<?php
/* @var $this PersonController */
/* @var $data Person */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
    <?php echo CHtml::encode($data->first_name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
    <?php echo CHtml::encode($data->last_name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('date_of_birth')); ?>:</b>
    <?php echo CHtml::encode($data->date_of_birth); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('zip_code')); ?>:</b>
    <?php echo CHtml::encode($data->zip_code); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
    <?php echo CHtml::encode(Yii::app()->dateFormatter->formatDateTime($data->created_at, "short")); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
    <?php echo CHtml::encode(Yii::app()->dateFormatter->formatDateTime($data->updated_at, "short")); ?>
    <br />


</div>